# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_namespace" "dyff_orchestrator" {
  metadata {
    name = "dyff-orchestrator"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

# https://artifacthub.io/packages/helm/dyff-orchestrator/dyff-orchestrator
resource "helm_release" "dyff_orchestrator" {
  name       = "dyff-orchestrator"
  namespace  = kubernetes_namespace.dyff_orchestrator.metadata[0].name
  repository = "oci://registry.gitlab.com/dyff/charts"
  chart      = "dyff-orchestrator"
  version    = "0.11.9"

  skip_crds = true

  wait          = false
  wait_for_jobs = false

  values = [yamlencode({
    args = [
      "--namespace=workflows",
      "--local_db_path=/var/dyff/orchestrator/data",
      "--debug_ignore_bad_messages",
      "--verbosity=1"
    ]

    persistence = {
      volumeSpec = {
        storageClassName = "regional-standard-rwo"
      }
    }

    extraEnvVarsConfigMap = {
      DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS = local.kafka.bootstrap_servers
      DYFF_KAFKA__TOPICS__WORKFLOWS_EVENTS  = local.kafka.topics_map["dyff.workflows.events"].name
      DYFF_KAFKA__TOPICS__WORKFLOWS_STATE   = local.kafka.topics_map["dyff.workflows.state"].name

      DYFF_ORCHESTRATOR__IMAGES__MOCK = "registry.gitlab.com/dyff/workflows/inferenceservice-mock:0.2.1"
      # !!! IMPORTANT !!!
      # Don't change the runner versions going forward! We now require these to
      # be set explicitly in the service spec, and these config options now
      # specify the default for legacy services that don't set an image
      # explicitly.
      # TODO: (DYFF-421) Remove these config options in schema v1
      DYFF_ORCHESTRATOR__IMAGES__HUGGINGFACE = "registry.gitlab.com/dyff/workflows/huggingface-runner:0.1.0"
      # TODO:
      # DYFF_ORCHESTRATOR__IMAGES__STANDALONE = "us-central1-docker.pkg.dev/dyff-354017/dyff-models/{service.id}:latest"
      DYFF_ORCHESTRATOR__IMAGES__VLLM = "registry.gitlab.com/dyff/workflows/vllm-runner:0.1.4"

      # GCloud supports FUSE for object storage interaction
      DYFF_SYSTEM__SUPPORTS_FUSE = "1"
    }

    replicaCount = 1

    resources = {
      limits = {
        cpu                 = "750m"
        memory              = "768Mi"
        "ephemeral-storage" = "1024Mi"
      }
      requests = {
        cpu                 = "500m"
        memory              = "512Mi"
        "ephemeral-storage" = "50Mi"
      }
    }
  })]
}
